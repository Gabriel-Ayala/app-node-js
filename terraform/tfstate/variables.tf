variable "aws_region" {
  type        = string
  description = "(optional) describe your variable"
  default     = "us-east-1"
}
variable "aws_profile" {
  type        = string
  description = "(optional) describe your variable"
  default     = "default"
}
variable "enviroment" {
  type        = string
  description = "(optional) describe your variable"
  default     = "dev"
}
variable "bucket_tag" {
  type        = map(string)
  description = "(optional) describe your variable"
  default = {
    name      = "tfstate"
    ManagedBy = "Terraform"
    Owner     = "Ayala"
  }
}