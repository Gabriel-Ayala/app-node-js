locals {
  env_region = split(".", terraform.workspace)
  env        = local.env_region[0]
  region     = local.env_region[1]

  key_name = "deployer-${local.env}"

  public_key_env = {
    development = {
      public_key = file("~/.ssh/id_rsa.pub")
    }
    production = {
      public_key = file("~/.ssh/id_rsa.pub")
    }
  }
  public_key = local.public_key_env[local.env].public_key

  context_instance_type = {
    development = {
      number = 1
      type   = "t2.medium"
    }
    production = {
      number = 1
      type   = "t2.medium"
    }
  }

  instance_type = local.context_instance_type[local.env].type
  instance_number = local.context_instance_type[local.env].number
}