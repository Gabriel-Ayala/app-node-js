terraform {
  backend "s3" {
    bucket  = "tfstate-803209982957"
    key     = "tfstate/terraform.tfsate"
    region  = "us-east-1"
    profile = "default"
  }
}