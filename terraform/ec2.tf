resource "aws_instance" "web_server" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = local.instance_type
  key_name      = aws_key_pair.deployer.key_name
  tags = {
    Name = "Web Server ${local.env}"
    Env  = local.env
  }
  depends_on = [
    aws_key_pair.deployer
  ]
}

resource "local_file" "hosts_cfg" {
  content = templatefile("./templates/hosts.tpl",
    {
      aws_ip = aws_instance.web_server.*.public_ip

    }
  )
  filename="../ansible/inventory/hosts"
}