resource "aws_key_pair" "deployer" {
  key_name   = local.key_name
  public_key = local.public_key
}