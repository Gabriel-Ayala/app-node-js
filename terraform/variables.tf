variable "environment" {
  type        = string
  description = ""
  default     = "dev"
}

variable "aws_region" {
  type = object({
    dev  = string
    prod = string
  })

  default = {
    dev  = "us-east-1"
    prod = "us-east-2"
  }
}
variable "aws_profile" {
  type        = string
  description = "(optional) describe your variable"
  default     = "default"
}
variable "instance" {
  type = object({
    dev = object({
      type   = string
      number = number
    })
    prod = object({
      type   = string
      number = number
    })
  })

  default = {
    dev = {
      number = 1
      type   = "t2.micro"
    }
    prod = {
      number = 1
      type   = "t2.medium"
    }
  }
}


variable "instance_tags" {
  type        = map(string)
  description = ""
  default = {
    Name       = "Web Server"
    Enviroment = "Staging"
    Owner      = "Ayala"

  }
}
